const mongoose = require("mongoose");

mongoose.connect("mongodb+srv://root:Deepu%401998@cluster0.svqvtw8.mongodb.net/test", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

var userSchema = mongoose.Schema({
    uid: String,
    token: String,
    email: String,
    name: String,
    gender: String,
    pic: String
});

module.exports = mongoose.model('User', userSchema);
